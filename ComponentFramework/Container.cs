﻿using System;
using System.Collections.Generic;

namespace ComponentFramework
{
    public class Container : IContainer
    {
        private ICollection<IComponent> components;
        private IDictionary<Type, IComponent> providedInterfaces;
        private IDictionary<Type, ICollection<IComponent>> unresolvedDependencies;

        public Container()
        {
            components = new HashSet<IComponent>();
            providedInterfaces = new Dictionary<Type, IComponent>();
            unresolvedDependencies = new Dictionary<Type, ICollection<IComponent>>();
        }

        public void RegisterComponent(IComponent component)
        {
            components.Add(component);
            ManageComponentDependencies(component);
        }

        public void RegisterComponents(IEnumerable<IComponent> components)
        {
            foreach (var component in components)
                RegisterComponent(component);
        }

        public void RegisterComponents(params IComponent[] components)
        {
            foreach (var component in components)
                RegisterComponent(component);
        }

        private void ManageComponentDependencies(IComponent component)
        {
            RegisterProvidedInterfaces(component);
            RegisterRequiredInterfaces(component);
            ResolveDependencies();
        }

        private void RegisterProvidedInterfaces(IComponent component)
        {
            foreach (var iface in component.ProvidedInterfaces)
                providedInterfaces[iface] = component;
        }

        private void RegisterRequiredInterfaces(IComponent component)
        {
            foreach (var iface in component.RequiredInterfaces)
            {
                ICollection<IComponent> unresolvedComponents;
                if (!unresolvedDependencies.TryGetValue(iface, out unresolvedComponents))
                {
                    unresolvedComponents = new HashSet<IComponent>();
                    unresolvedDependencies[iface] = unresolvedComponents;
                }
                unresolvedComponents.Add(component);
            }
        }

        private void ResolveDependencies()
        {
            var unresolvedCopy = new Dictionary<Type, ICollection<IComponent>>(unresolvedDependencies);
            foreach (var iface in unresolvedCopy.Keys)
            {
                var interfaceImplementation = GetInterface(iface);
                if (interfaceImplementation != null)
                {
                    foreach (var component in unresolvedDependencies[iface])
                        component.InjectInterface(iface, interfaceImplementation);
                    unresolvedDependencies.Remove(iface);
                }
            }
        }

        public bool DependenciesResolved
        {
            get { return unresolvedDependencies.Count == 0; }
        }

        public IDictionary<Type, ICollection<IComponent>> UnresolvedDependencies
        {
            get { return unresolvedDependencies; }
        }

        public object GetInterface(Type iface)
        {
            IComponent providingComponent;
            if (providedInterfaces.TryGetValue(iface, out providingComponent))
            {
                return providingComponent.GetInterface(iface);
            }
            return null;
        }

        public T GetInterface<T>() where T : class
        {
            Type type = typeof(T);
            return GetInterface(type) as T;
        }
    }
}
